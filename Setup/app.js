new Vue({
    el: "#app",
    data: {
        playHealth: 100,
        monsterHealth: 100,
        gameRunning: false,
        turns: []
    },
    methods: {
        startGame: function () {
            this.gameRunning = true;
            this.playHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        attack: function () {
            var damage = this.calculateDamage(3, 10)

            this.monsterHealth -= damage;
            this.turns.unshift({
                isPlayer: true,
                text: "Player hits Monster for " + damage
            })

            if (this.checkWin()) {
                return;
            }

            this.monsterAttacks();
        },
        specialATTACK: function () {
            var damage = this.calculateDamage(10, 20)
            this.monsterHealth -= damage;
            this.turns.unshift({
                isPlayer: true,
                text: "Player hits Monster hard for " + damage
            })
            if (this.checkWin()) {
                return;
            }
            this.monsterAttacks();
        },
        heal: function () {
            if (this.playHealth <= 90) {
                this.playHealth += 10;
            } else {
                this.playHealth = 100
            }
            this.turns.unshift({
                isPlayer: true,
                text: "Player heal for 10"
            })
            this.monsterAttacks();
        },
        giveUP: function () {
            this.gameRunning = false
        },
        monsterAttacks: function () {
            var damage = this.calculateDamage(5, 12)
            this.playHealth -= damage;
            this.checkWin()
            this.turns.unshift({
                isPlayer: false,
                text: "Monster hits Player for " + damage
            })
        },
        calculateDamage: function (min, max) {
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        checkWin: function () {
            if (this.monsterHealth <= 0) {
                if (confirm("You won! New Game?")) {
                    this.startGame();
                } else {
                    this.gameRunning = false;
                }
                return true;
            } else if (this.playHealth <= 0) {
                if (confirm("You lost! New Game?")) {
                    this.startGame();
                } else {
                    this.gameRunning = false;
                }
                return true
            }
            return false
        }
    },
})